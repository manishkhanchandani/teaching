function Queue() {
  this.data = [];

  this.insert = function (val) {
    this.data.unshift(val);
  };

  this.remove = function () {
    return this.data.pop();
  };

  this.size = function () {
    return this.data.length;
  };

  this.peek = function () {
    return this.data[this.data.length - 1];
  };
}

const q = new Queue();
q.insert('person1');
q.insert('person2');
q.insert('person3');
q.insert('person4');
q.insert('person5');

q.size();
q.peek();
q.remove();

q.size();
q.peek();
q.remove();

q.size();
q.peek();
q.remove();

q.size();
q.peek();
q.remove();

q.size();
q.peek();
q.remove();

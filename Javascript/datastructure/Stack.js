function Stack() {
  this.data = [];

  this.insert = function (val) {
    this.data.push(val);
  };

  this.remove = function () {
    return this.data.pop();
  };

  this.size = function () {
    return this.data.length;
  };

  this.peek = function () {
    return this.data[this.data.length - 1];
  };
}

var obj = new Stack();
obj.insert('book1');
obj.insert('book2');
obj.insert('book3');
obj.insert('book4');
obj.insert('book5');

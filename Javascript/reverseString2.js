//Cat
function reverseString(str) {
  // create a new variable
  let newStr = '';
  // loop through str variable and put each character before the previour character
  for (let i = 0; i < str.length; i++) {
    newStr = str[i] + newStr;
    // 'c'
    // 'ac'
    // 'tac'
  }
  // Returning my new string with reverse of str
  return newStr;
}
reverseString('cat');
// "tac"

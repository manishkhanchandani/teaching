// Given array and size, divide the array into many subarrays
// Where as each subarray is of length size.
function chunk(arr, size) {
  const arr2 = [];
  while (arr.length > 0) {
    const x = arr.splice(0, size);
    arr2.push(x);
  }
  return arr2;
}

chunk([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 4);
/* output:
0: (4) [1, 2, 3, 4]
1: (4) [5, 6, 7, 8]
2: (2) [9, 10]
*/

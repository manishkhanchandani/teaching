function twoSum(nums, target) {
  var obj = {};
  for (var i = 0; i < nums.length; i++) {
    var num = nums[i]; // 5
    if (obj[num] !== undefined) {
      return [obj[num], i]; // [3, 4]
    } else {
      obj[target - num] = i; // 5 = 3
    }
  }

  return null;
}
// obj = { 8: 0, 7: 1, 6: 2, 5: 3,  }

var nums = [1, 2, 3, 4, 5];
var target = 9;
twoSum(nums, target);
//[3, 4]

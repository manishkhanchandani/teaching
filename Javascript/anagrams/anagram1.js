// Anagrams
// Compare the 2 string,
// string 1 has all characters which are also in string 2, then we say it is anagram.
//'abc', 'cba'
// 'cat', 'dog'
// Method 1

function anagram(str1, str2) {
  // Step 1: convert to array
  // Step 2: Sort the array
  // Step 3: convert to string
  const a = str1.split('').sort().join('');
  const b = str2.split('').sort().join('');
  return a === b;
}

anagram('abc', 'cba');
// true
anagram('cat', 'dog');
// false

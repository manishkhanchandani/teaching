//var obj1 = {};
//cat
//obj1[c] = 1
//obj1[a] = 1
//obj1[t] = 1
//tac
//obj2[t] = 1
//obj2[a] = 1
//obj2[c] = 1
//obj1[c] === obj2[c]
//obj1[a] === obj2[a]
//obj1[t] === obj2[t]
function mapping(str) {
  const obj = {};
  for (let i = 0; i < str.length; i++) {
    const char = str[i];
    if (!obj[char]) {
      obj[char] = 1;
    } else {
      obj[char] = obj[char] + 1;
    }
  }
  return obj;
}
function anagram(str1, str2) {
  const obj1 = mapping(str1);
  const obj2 = mapping(str2);
  if (Object.keys(obj1).length !== Object.keys(obj2).length) {
    return false;
  }

  for (let char in obj1) {
    if (obj1[char] !== obj2[char]) {
      return false;
    }
  }
  return true;
}

anagram('abc', 'cba');
// true
anagram('cat', 'dog');
// false

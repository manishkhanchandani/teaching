function palindrome(str) {
  var len = str.length;
  for (let i = 0; i < str.length; i++) {
    if (str[i] !== str[len - 1 - i]) {
      return false;
    }
  }
  return true;
}

palindrome('abba');
// true

palindrome('cat');
// false

function palindrome(str) {
  var len = str.length;
  return str.split('').every((character, index) => {
    //return true or false
    return character === str[len - 1 - index];
  });
}

palindrome('abba');
// true

palindrome('cat');
// false

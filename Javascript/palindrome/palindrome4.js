function palindrome(str) {
  const len = str.length;
  const midpoint = len % 2 === 0 ? len / 2 : (len + 1) / 2;
  console.log('length is ', len, ', and midpoint is ', midpoint);
  return str.split('').every((char, i) => {
    if (i >= midpoint) {
      return true;
    }
    console.log(
      'index is ',
      i,
      ', char is ',
      char,
      ', and char 2 is ',
      str[len - 1 - i]
    );
    return char === str[len - 1 - i];
  });
}
//abba -- here remainder is zero so it is even
//abcba -- remainder as non zero and so it is odd

palindrome('abba');
// true

palindrome('cat');
// false

function add(x, y) {
  var sum = x;
  if (arguments.length > 1) {
    return sum + y;
  } else {
    return function result(y) {
      return sum + y;
    };
  }
}
//add(3,5); // 8
//add(3)(5); // 8

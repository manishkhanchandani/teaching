// sum(1, 2) we have to return result from cache if it exist else it should calculate the sum and return the sum.
//sum(1,2); // 3
//sum(1,2); //cache 3

function sum() {
  var cache = {};
  return function (x, y) {
    var key = x + '-' + y;
    if (cache[key]) {
      console.log('cache exist ', cache[key]);
      return cache[key];
    } else {
      cache[key] = x + y;
      console.log('cache does not exist ');
      return cache[key];
    }
  };
}

var sum = sum();

sum(1, 2);
// cache does not exist
// 3
sum(1, 2);
// cache exist  3
// 3
sum(2, 3);
// cache does not exist
// 5
sum(2, 3);
// cache exist  5
// 5

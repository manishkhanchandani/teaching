function add(x) {
  var sum = x;
  return function result(y) {
    if (arguments.length > 0) {
      sum = sum + y;
      return result;
    } else {
      // when no value is passed as y
      return sum;
    }
  };
}

add(1)(2)();
//3
add(1)(2)(3)();
//6
add(1)(2)(3)(4)();
//10

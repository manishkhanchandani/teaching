//[0,1,1,2,3,5,8,13,21,34]
//Fibonacci series

function fib(n) {
  const arr = [0, 1]; // first 2 numbers are fixed
  for (let i = 2; i <= n; i++) {
    arr.push(arr[i - 1] + arr[i - 2]);
  }
  return arr;
}

fib(4);
//[0, 1, 1, 2, 3]

// Flatten an array of arrays
function flatten(arr) {
  return arr.reduce((acc, cur, index) => {
    return acc.concat(cur);
  }, []);
}

flatten([
  [0, 1],
  [2, 3],
  [4, 5]
]);
//output: (6) [0, 1, 2, 3, 4, 5]

function removeDuplicateItems(arr) {
  return arr.reduce((acc, cur, i) => {
    if (acc.indexOf(cur) === -1) {
      acc.push(cur); //['a', 'b', 'c', 'e', 'd']
    }
    return acc;
  }, []);
}

var myArray = ['a', 'b', 'a', 'b', 'c', 'e', 'e', 'c', 'd', 'd', 'd', 'd', 'd'];

removeDuplicateItems(myArray);

//Output ['a', 'b', 'c', 'e', 'd']

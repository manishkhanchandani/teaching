function reverseString(str) {
  return str.split('').reduce((acc, cur) => {
    return cur + acc;
  }, '');
}

reverseString('cat');

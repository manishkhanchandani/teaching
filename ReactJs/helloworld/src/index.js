import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from './store';
import App from './App';
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.min.css';

const store = configureStore();

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
/*
import { createStore } from 'redux';

const reducer = (state = 1, action = {}) => {
  switch (action.type) {
    case 'ADD':
      state = state + action.payload;
      break;
    case 'SUBTRACT':
      state = state - action.payload;
      break;
    case 'INCREMENT':
      state = state + 1;
      break;
    case 'DECREMENT':
      state = state - 1;
      break;
    default:
      break;
  }
  return state;
};

const store = createStore(reducer);

store.subscribe(() => {
  console.log('store update: ', store.getState());
});

store.dispatch({
  type: 'ADD',
  payload: 10
});

store.dispatch({
  type: 'ADD',
  payload: 5
});
store.dispatch({
  type: 'ADD',
  payload: 6
});
store.dispatch({
  type: 'SUBTRACT',
  payload: 10
});
store.dispatch({
  type: 'INCREMENT'
});
store.dispatch({
  type: 'INCREMENT'
});
store.dispatch({
  type: 'INCREMENT'
});
store.dispatch({
  type: 'DECREMENT'
});

// Component - View
// Dispatch - Action
// Middlewares - functionalities which helps in doing different stuff like ajax call.
// Reducer
// Store
// Component.
*/

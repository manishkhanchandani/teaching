const initialState = {
  data: [
    /*{
      id: 1,
      task: 'Go to market',
      uid: 1
    },
    {
      id: 2,
      task: 'Read or watch youtube online class',
      uid: 1
    }*/
  ]
};

const ToDoReducer = (state = initialState, action = {}) => {
  switch (action.type) {
    case 'ADD':
      state = {
        ...state,
        data: [...state.data, action.payload]
      };
      break;
    case 'DELETE':
      const index = state.data.findIndex((elem) => {
        return elem.id === action.payload;
      });
      const deleteData = [...state.data];
      deleteData.splice(index, 1);
      state = {
        ...state,
        data: deleteData
      };
      break;
    default:
      break;
  }

  return state;
};

export default ToDoReducer;

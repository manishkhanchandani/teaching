import React from 'react';
import { connect } from 'react-redux';
import { addToDo, deleteToDo } from './ToDoAction';
import Input from '../../widgets/Input';

const mapState = (state) => {
  return {
    toDoReducer: state.toDoReducer
  };
};

const actions = {
  addToDo,
  deleteToDo
};

const ToDo = (props) => {
  const [task, setTask] = React.useState('');
  const handleChange = (e) => {
    setTask(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const obj = {
      id: Date.now().toString(),
      task: task,
      uid: 1
    };
    props.addToDo(obj);
    setTask('');
  };
  return (
    <div>
      <h3>Todo App</h3>
      <form onSubmit={handleSubmit}>
        <Input
          handleChange={handleChange}
          name="task"
          title="Task"
          placeholder="Enter Your To do task"
          size="sm"
          value={task}
        />
      </form>
      {props.toDoReducer.data.length > 0 && (
        <>
          <h3>View My Todo Items</h3>
          <table className="table">
            <thead className="thead-dark">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Task</th>
                <th scope="col">Delete</th>
              </tr>
            </thead>
            <tbody>
              {props.toDoReducer.data.map((item) => {
                return (
                  <tr key={item.id}>
                    <th scope="row">{item.id}</th>
                    <td>{item.task}</td>
                    <td>
                      <span
                        onClick={() => {
                          props.deleteToDo(item.id);
                        }}
                        style={{ cursor: 'pointer' }}>
                        Delete
                      </span>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </>
      )}
    </div>
  );
};

export default connect(mapState, actions)(ToDo);

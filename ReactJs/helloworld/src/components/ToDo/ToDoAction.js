export const addToDo = (data) => {
  return (dispatch) => {
    dispatch({ type: 'ADD', payload: data });
  };
};

export const deleteToDo = (id) => {
  return (dispatch) => {
    dispatch({ type: 'DELETE', payload: id });
  };
};

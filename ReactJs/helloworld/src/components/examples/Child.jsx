import React from 'react';
import PropTypes from 'prop-types';

function Child(props) {
  console.log('props', props);
  return (
    <div>
      <h1>
        Child - {props.title} - {props.description}
      </h1>
    </div>
  );
}

Child.defaultProps = {
  title: 'Hello World',
  description: 'This is description'
};

Child.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string
};

export default Child;

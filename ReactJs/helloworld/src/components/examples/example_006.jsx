import React from 'react';
/*
Refs are a special attribute that are available on all React components. They allow us to create a reference to a given element when the component mounts.

useRef allows us to easily use React refs.

We call useRef (at top of component) and attach the returned value to the element's ref attribute to refer to it.

Once we create a reference, we use the current property to modify (mutate) the elements properties or can call any other function related to that element.

const a = React.useRef(0);
a.current = 0
*/

const View1 = () => {
  const [query, setQuery] = React.useState('some value');
  const searchInput = React.useRef(null);
  const handleBtn = () => {
    searchInput.current.value = '';
    searchInput.current.focus();
    searchInput.current.style.backgroundColor = '#ffcc00';
    console.log(searchInput);
  };
  return (
    <div>
      <h3>View1</h3>
      <input
        type="text"
        ref={searchInput}
        value={query}
        onChange={(e) => setQuery(e.target.value)}
      />
      <button onClick={handleBtn}>Handle</button>
    </div>
  );
};

const View2 = () => {
  const [name, setName] = React.useState('');
  const count = React.useRef(0);
  React.useEffect(() => {
    count.current = count.current + 1;
  });
  return (
    <div>
      <h3>View2</h3>
      <div>My name is {name}</div>
      <input
        value={name}
        onChange={(e) => {
          setName(e.target.value);
        }}
      />
      <div>Rerender count: {count.current}</div>
    </div>
  );
};

const View3 = () => {
  const ref1 = React.useRef(null);
  const ref2 = React.useRef(null);
  const ref3 = React.useRef(null);
  React.useEffect(() => {
    ref1.current.focus();
  }, []);
  return (
    <div>
      <h3>View3</h3>
      <div>
        <input
          placeholder="enter first name"
          onKeyDown={(e) => {
            if (e.key === 'Enter') {
              ref2.current.focus();
            }
          }}
          type="text"
          ref={ref1}
        />
      </div>
      <div>
        <input
          placeholder="enter midle name"
          onKeyDown={(e) => {
            if (e.key === 'Enter') {
              ref3.current.focus();
            }
          }}
          type="text"
          ref={ref2}
        />
      </div>
      <div>
        <input
          placeholder="enter last name"
          onKeyDown={(e) => {
            if (e.key === 'Enter') {
              ref1.current.focus();
            }
          }}
          type="text"
          ref={ref3}
        />
      </div>
    </div>
  );
};

function Example() {
  return (
    <div>
      <h1>Example</h1>
      <View3 />
      <hr />
      <View2 />
      <hr />
      <View1 />
      <hr />
    </div>
  );
}

export default Example;

import React from 'react';

function Example() {
  const [user, setUser] = React.useState(null);
  // Ajax Request in react, GET, POST, PUT and DELETE,
  // https://jsonplaceholder.typicode.com/users
  React.useEffect(() => {
    // async and await
    /*fetch(endpoint).then((response) => {
        return response.json();
    }).then((data) => {
        setUser(data);
    });*/
    const process = async () => {
      const endpoint = 'https://jsonplaceholder.typicode.com/users';
      const response = await fetch(endpoint);
      const data = await response.json();
      setUser(data);
    };

    process();
  }, []); // this use effect will be called only once because we passed second parameter as an empty array.\n *

  console.log('user is ', user);
  return (
    <div>
      <h3>Users</h3>
      {user &&
        user.map((item) => {
          return (
            <div key={item.id}>
              {item.name}, {item.address.city}, {item.company.name}
            </div>
          );
        })}
    </div>
  );
}

export default Example;

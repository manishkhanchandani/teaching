import React from 'react';

const Example = (props) => {
  console.log('props: ', props);
  const id = props.match.params.id;
  const [counter, setCounter] = React.useState(0);
  const [name, setName] = React.useState('John');
  const [sortingField, setSortingField] = React.useState('John');
  React.useEffect(() => {
    console.log('on page load or on component load');
  }, []);

  React.useEffect(() => {
    console.log('counter is changed to ', counter);
  }, [counter]);

  React.useEffect(() => {
    console.log('name is changed');
  }, [name]);

  React.useEffect(() => {
    console.log('id');
  }, [id]);

  React.useEffect(() => {
    console.log('table header which is clicked is ', sortingField);
  }, [sortingField]);

  return (
    <div>
      <h1>Counter</h1>
      <div>
        {name}, Value of counter is {counter}
      </div>
      <button
        onClick={() => {
          setCounter(counter + 1);
        }}>
        Increase Counter
      </button>
      <button
        onClick={() => {
          setName('Bob');
        }}>
        Change Name
      </button>

      <button
        onClick={() => {
          setSortingField('name');
        }}>
        Name is Clicked
      </button>
      <button
        onClick={() => {
          setSortingField('age');
        }}>
        Age is Clicked
      </button>
    </div>
  );
};

export default Example;

import React from 'react';

const Person = (props) => {
  return <li>this person's name is {props.item.name}</li>;
};

const Example = () => {
  const people = [
    {
      name: 'John'
    },
    {
      name: 'Bob'
    },
    {
      name: 'Fred'
    }
  ];
  return (
    <div style={{ margin: '20px' }}>
      <h1>List of Items</h1>
      <ul>
        {people &&
          people.map((item, index) => {
            return <Person key={index} item={item} />;
          })}
      </ul>
    </div>
  );
};

export default Example;

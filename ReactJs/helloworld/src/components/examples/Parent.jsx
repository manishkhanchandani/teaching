import React from 'react';
import Child from './Child';
function Parent() {
  return (
    <div>
      <h1>Parent</h1>
      <Child title="hello" description="this is new desc" />
    </div>
  );
}

export default Parent;

import React from 'react';
import { connect } from 'react-redux';
import { action1, action2 } from '../../actions/TestAction1';
import { action3, action4 } from '../../actions/TestAction2';

const mapState = (state) => {
  console.log('state is ', state);
  return {
    t1: state.t1,
    t2: state.t2
  };
};

const actions = {
  action1,
  action2,
  action3,
  action4
};

const Example = (props) => {
  console.log('props are: ', props);
  return (
    <div>
      <h1>Example</h1>
      <button
        onClick={() => {
          props.action3();
        }}>
        Increment
      </button>
      <div>Result is: {props.t2.result}</div>
    </div>
  );
};

export default connect(mapState, actions)(Example);

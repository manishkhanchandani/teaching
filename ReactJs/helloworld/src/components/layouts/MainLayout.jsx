import React from 'react';
import Nav from '../pages/Nav';

const MainLayout = (props) => {
  return (
    <div className="container">
      <h1>Main Layout Page</h1>
      <Nav />
      <hr />
      {props.children}
      <hr />
      <div>This is footer.</div>
    </div>
  );
};

export default MainLayout;

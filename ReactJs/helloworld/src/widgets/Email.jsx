import React from 'react';
import { Form } from 'react-bootstrap';

const Email = (props) => {
  return (
    <Form.Group controlId={props.name}>
      <Form.Label>Email address</Form.Label>
      <Form.Control
        type="email"
        placeholder="Enter email"
        onChange={props.handleChange}
        name="email"
      />
      <Form.Text className="text-muted">
        We'll never share your email with anyone else.
      </Form.Text>
    </Form.Group>
  );
};

export default Email;

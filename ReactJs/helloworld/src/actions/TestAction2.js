export const action3 = () => {
  return async (dispatch, getState) => {
    dispatch({
      type: 'INCREMENT'
    });
  };
};

export const action4 = () => {
  return async (dispatch, getState) => {
    dispatch({
      type: 'DECREMENT'
    });
  };
};

import { combineReducers } from 'redux';
import TestReducer1 from './TestReducer1';
import TestReducer2 from './TestReducer2';
import ToDoReducer from '../components/ToDo/ToDoReducer';

const rootReducer = combineReducers({
  t1: TestReducer1,
  t2: TestReducer2,
  toDoReducer: ToDoReducer
});

export default rootReducer;

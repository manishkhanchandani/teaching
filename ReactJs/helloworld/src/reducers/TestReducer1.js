const initialState = {
  data1: null,
  data2: null,
  result: 0
};

const TestReducer1 = (state = initialState, action = {}) => {
  switch (action.type) {
    case 'ADD':
      state = {
        ...state,
        result: state.result + action.payload
      };
      break;
    case 'SUBTRACT':
      state = {
        ...state,
        result: state.result - action.payload
      };
      break;
    case 'DATA1':
      state = { ...state, data1: action.payload };
      break;
    case 'DATA2':
      state = { ...state, data2: action.payload };
      break;
    default:
      break;
  }

  return state;
};

export default TestReducer1;

import React from 'react';
import { Form, Button } from 'react-bootstrap';

import Email from './widgets/Email';
import Password from './widgets/Password';
import Input from './widgets/Input';
import Textarea from './widgets/Textarea';
import RichTextarea from './widgets/RichTextarea';
import Select from './widgets/Select';

import Cmp01 from './widgets/ReactSelect/Cmp01';
import Cmp02 from './widgets/ReactSelect/Cmp02';

function App() {
  const [formValues, setFormValues] = React.useState({});

  const handleDescription = (input) => {
    setFormValues({ ...formValues, description: input });
  };

  const handleChange = (e) => {
    // write some code to update form values
    setFormValues({ ...formValues, [e.target.name]: e.target.value });
  };

  const handleSubmit = (e) => {
    e.preventDefault(); // form submission will not cause page refresh
    console.log('final submitted object is ', formValues);
    // write object to database or firebase or parse server or mysql or mongodb
  };

  const handleSelect = (name, e) => {
    setFormValues({ ...formValues, [name]: e });
  };

  console.log('formValues: ', formValues);
  return (
    <div className="container">
      <h1>Form</h1>
      <Form onSubmit={handleSubmit}>
        <Email handleChange={handleChange} />
        <Password
          handleChange={handleChange}
          name="password"
          title="Password"
          placeholder="Enter Password"
        />
        <Password
          handleChange={handleChange}
          name="confirm_password"
          title="Confirm Password"
          placeholder="Enter Confirm Password"
        />
        <Input
          handleChange={handleChange}
          name="first_name"
          title="First Name"
          placeholder="Enter First Name"
          size="lg"
          readOnly={false}
          plaintext={false}
          value={formValues.first_name || ''}
        />
        <Input
          handleChange={handleChange}
          name="middle_name"
          title="Middle Name"
          placeholder="Enter Middle Name"
          readOnly={false}
          plaintext={true}
        />
        <Input
          handleChange={handleChange}
          name="last_name"
          title="Last Name"
          placeholder="Enter Last Name"
          size="sm"
          readOnly={false}
          plaintext={false}
        />
        <Textarea
          handleChange={handleChange}
          name="details"
          title="Details"
          placeholder="Enter Details"
        />
        <RichTextarea
          handleDescription={handleDescription}
          title="Description"
          placeholder="Enter Description"
        />
        <Select
          size="lg"
          handleChange={handleChange}
          name="gender"
          title="Gender"
        />

        <Cmp01
          handleChange={handleSelect.bind(this, 'cpm01')}
          title="Sample 1"
          name="cmp01"
        />
        <Cmp02 title="Sample 1" name="cmp04" />

        <Button variant="primary" type="submit">
          Submit
        </Button>
        <Button
          variant="primary"
          onClick={() => {
            setFormValues({});
          }}>
          Reset
        </Button>
      </Form>
    </div>
  );
}

export default App;

import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import rootReducer from './reducers/rootReducer';

const configureStore = (preloadedState) => {
  const middlewares = [thunk, logger];
  const middleWareEnhancer = applyMiddleware(...middlewares);
  const storeEnhancers = [middleWareEnhancer];
  const composedEnhancer = composeWithDevTools(...storeEnhancers);
  const store = createStore(rootReducer, preloadedState, composedEnhancer);
  return store;
};

export default configureStore;

import React from 'react';

function Home4() {
  const [language, setLanguage] = React.useState('python');
  const [yearsExperience, setYearsExperience] = React.useState(0);
  /*const [developer, setDeveloper] = React.useState({
    language: 'python',
    yearsExperience: 20
  });*/

  //   React.useEffect(() => {
  //     console.log('This will called only on load of the component ');
  //   }, []);

  React.useEffect(() => {
    console.log(
      'This will called only on there is change in yearsExperience ',
      yearsExperience,
      ', ',
      language
    );
  }, [yearsExperience, language]);

  //   const handleChange = (e) => {
  //     setDeveloper({ ...developer, yearsExperience: e.target.value });
  //   };
  return (
    <div>
      <br />
      <button
        onClick={() => {
          setLanguage('java');
        }}>
        Change Language
      </button>
      <br />
      <input
        type="text"
        value={yearsExperience}
        onChange={(e) => {
          setYearsExperience(e.target.value);
        }}
      />
      <br />I am learning {language}
      <br />I have {yearsExperience} years of experience.
    </div>
  );
}

export default Home4;

import React from 'react';

function Home2(props) {
  // props.handleTitle
  return (
    <div>
      {props.name} - {props.age} years
      <button
        onClick={() => {
          props.handleTitle(props.name);
        }}>
        Update My Name
      </button>
    </div>
  );
}

export default Home2;

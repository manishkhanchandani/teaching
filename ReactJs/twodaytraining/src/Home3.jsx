import React from 'react';

function Home() {
  // will i create 20 such states.
  const [formValues, setFormValues] = React.useState({});
  const handleChange = (e) => {
    e.persist();
    setFormValues((previousValue) => {
      return { ...previousValue, [e.target.name]: e.target.value };
    });
  };
  console.log('formValues is ', formValues);
  return (
    <div>
      <h1>Hello world</h1>
      <input
        type="text"
        name="first_name"
        placeholder="Enter First Name"
        value={formValues.first_name || ''}
        onChange={handleChange}
      />
      <br />
      <input
        type="text"
        name="last_name"
        placeholder="Enter Last Name"
        value={formValues.last_name || ''}
        onChange={handleChange}
      />
      <br />
      <input
        type="text"
        name="middle_name"
        placeholder="Enter Middle Name"
        value={formValues.middle_name || ''}
        onChange={handleChange}
      />
      <br />
    </div>
  );
}

export default Home;

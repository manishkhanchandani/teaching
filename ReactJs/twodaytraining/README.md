React is a javascript framework
It is faster, has great user experience.
Virtual Dom,

Step 1: To create a project,
npx create-react-app twodaytraining

npm install --save react-bootstrap bootstrap redux react-redux react-router-dom redux-devtools-extension redux-thunk redux-logger

Topic is Elements and JSX
Put html inside the javascript. JSX
.jsx, .js

1. Elements & Components JSX
2. Props
3. List and keys
4. UseState
5. Events
6. UseEffect

Components - Capital letter HelloWorld

<HelloWorld />

Input
<Input />
input
<input />

JSX elements are expressions and can be assigned to variables:

const val1 = <div>Hello World</div>;

const isNewToReact = true;

if (isNewToReact) {
return val1;
} else {
return <div>Hi again, React</div>
}

const year = 2020;
const greeting = <div>Hello React in {year}</div>
string, number, array, bool

object
const obj = {};
const greeting = <div>Hello React in {obj}</div>

//we need some root element
const greeting = <div><h1>Title</h1><div>Hello React in {year}</div></div>

// empty div <div></div>
// <input />
<button className="submit-button">Submit</button>

Most basic React app required following three things:
ReactDom.render() - to render our application
A JSX element (called a root node);
A Dom element within which to mount the app.

import 'bootstrap/dist/css/bootstrap.min.css';

React single page application, no refreshing of the page,
One to another , refresh the content part.

1. UseEffect
2. Add, Edit, Delete, View (List)
   Things I like

Redux Live, i will only when it is required

UseEffect:

1. Every time whenever there is change in state or props:
   React.useEffect(() => {
   // my code will go here.
   });

2. I want to call useEffect only one time when component is loaded.
   React.useEffect(() => {
   // my code will go here
   }, []);

3. I want to call use Effect only when certain variable is changed:
   React.useEffect(() => {
   // some code is here
   }, [var1]);

React.useEffect(() => {
// some code is here
}, [var2]);

React.useEffect(() => {
// some code is here
}, [var1, var2]);

Movies
Cricket

Things I like
Step 1. Following Files
List, Add, Edit, Delete
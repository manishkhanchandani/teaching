var express = require('express');
var ParseServer = require('parse-server').ParseServer;
var ParseDashboard = require('parse-dashboard');
var path = require('path');
var app = express();

//parse-server-dedicated-email-adapter

var databaseUri = 'mongodb://localhost:27017/completereact'; //'mongodb+srv://manishkk74:<password>@cluster0.9rsqp.mongodb.net/completereact?retryWrites=true&w=majority'
var server_url = 'http://localhost:1337/parse';

var api = new ParseServer({
  databaseURI: databaseUri,
  cloud: __dirname + '/cloud/main.js',
  appId: 'myAppId',
  masterKey: 'myMasterKey',
  serverURL: server_url,
  liveQuery: {
    classNames: ['TestingAll']
  },
  // Enable email verification
  verifyUserEmails: true,
  preventLoginWithUnverifiedEmail: true,
  publicServerURL: server_url,
  appName: 'Facebook',
  emailAdapter: {
    module: 'parse-server-dedicated-email-adapter',
    options: {
      host: 'smtp.gmail.com',
      port: 465,
      secure: true,
      email: 'teachingreactjs@gmail.com',
      password: 'passwords1234567' //https://myaccount.google.com/security, Less secure app access to on
    }
  }
});

// for static
app.use('/public', express.static(path.join(__dirname, '/public')));

// calling the parse
app.use('/parse', api);

// Dashboard Settings:
var options = { allowInsecureHTTP: false };
var dashboard = new ParseDashboard(
  {
    apps: [
      {
        serverURL: server_url,
        appId: 'myAppId',
        masterKey: 'myMasterKey',
        appName: 'Complete React App'
      }
    ],
    users: [
      {
        user: 'manishkk74@gmail.com',
        pass: 'password'
      }
    ],
    trustProxy: 1
  },
  options
);
app.use('/dashboard', dashboard);

// routing
app.get('/', (req, res) => {
  res.send('Parse Server is up again.');
});
app.get('/test', (req, res) => {
  res.sendFile(path.join(__dirname, '/public/test.html'));
});

var port = 1337;
var httpServer = require('http').createServer(app);
httpServer.listen(port, () => {
  console.log('parse server is running on port ' + port);
});

// Live Query:
ParseServer.createLiveQueryServer(httpServer);
